static const char col_bar_bg_norm[] = "#000000";
static const char col_bar_bg_sel[]  = "#1C1C1C";
static const char col_bar_fg_norm[] = "#FFFFD7";
static const char col_bar_fg_sel[]  = "#FFFFD7";
static const char col_border_norm[] = "#1C1C1C";
static const char col_border_sel[]  = "#81A2BE";
static const char col_border_urg[]  = "#D54E53";
