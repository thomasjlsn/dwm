static const char col_bar_bg_norm[] = "#073642";
static const char col_bar_bg_sel[]  = "#000000";
static const char col_bar_fg_norm[] = "#FCF5E2";
static const char col_bar_fg_sel[]  = "#073642";
static const char col_border_norm[] = "#073642";
static const char col_border_sel[]  = "#EDE7D4";
static const char col_border_urg[]  = "#CA4B16";
