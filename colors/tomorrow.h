static const char col_bar_bg_norm[] = "#121212";
static const char col_bar_bg_sel[]  = "#000000";
static const char col_bar_fg_norm[] = "#BBBBBB";
static const char col_bar_fg_sel[]  = "#EEEEEE";
static const char col_border_norm[] = "#444444";
static const char col_border_sel[]  = "#282828";
static const char col_border_urg[]  = "#FF3334";
