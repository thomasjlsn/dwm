/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;       /* size of gaps between windows */
static const unsigned int gapmax    = 50;       /* max size of gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int gapsnap            = 1;        /* 1 means window snapping respects gaps */
static const int smartgaps          = 0;        /* 1 means disable gaps & borders when only one client is visable */
static const int rmaster            = 1;        /* 1 means master-area is initially on the right */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "Fira Code:size=10" };
static const char dmenufont[]       = "Fira Code:size=10";

#include "colors/thomas.h"
static const char *colors[][3] = {
	[SchemeNorm] = { col_bar_fg_norm, col_bar_bg_norm, col_border_norm },
	[SchemeSel]  = { col_bar_fg_sel,  col_bar_bg_sel,  col_border_sel },
	[SchemeUrg]  = { col_bar_bg_sel,  col_bar_fg_sel,  col_border_urg  },
};

/* opacity settings
 *
 * Use the floats under value to change opacity.
 *   1.00 -> opaque
 *   0.00 -> transparent
 */                                          /* value */
static const unsigned int bgalpha     = 0xffU * 0.90;
static const unsigned int fgalpha     = 0xffU * 0.90;
static const unsigned int borderalpha = 0xffU * 1.00;

static const unsigned int alphas[][3]      = {
	/*               fg       bg       border     */
	[SchemeNorm] = { fgalpha, bgalpha, borderalpha },
	[SchemeSel]  = { fgalpha, bgalpha, borderalpha },
};

/* sticky icon */
static const XPoint stickyicon[]    = { {0,0}, {4,0}, {4,8}, {2,6}, {0,8}, {0,0} }; /* represents the icon as an array of vertices */
static const XPoint stickyiconbb    = {4,8}; /* defines the bottom right corner of the polygon's bounding box (speeds up scaling) */

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class            instance    title           tags mask  isfloating  isterminal   noswallow  monitor */
	{ "Gimp",           NULL,       NULL,           0,         0,          0,           0,         -1 },
	{ "Firefox",        NULL,       NULL,           0,         0,          0,           0,         -1 },
	{ "Pavucontrol",    NULL,       NULL,           0,         1,          0,           0,         -1 },
	{ "Gpick",          NULL,       NULL,           0,         1,          0,           0,         -1 },

	/* terminal emulators */
	{ "alacritty",      NULL,       NULL,           0,         0,          1,           -1,         -1 },
	{ "Terminal",       NULL,       NULL,           0,         0,          1,           -1,         -1 },
	{ "st",             NULL,       NULL,           0,         0,          1,           -1,         -1 },
	{ "xterm",          NULL,       NULL,           0,         0,          1,           -1,         -1 },
	{ "xterm-256color", NULL,       NULL,           0,         0,          1,           -1,         -1 },

	/* don't swallow xev */
	{ NULL,             NULL,       "Event Tester", 0,         1,          0,           1,          -1 },
};

/* layout(s) */
static const float mfact        = 0.35; /* factor of master area size [0.05..0.95] */
static const int nmaster        = 1;    /* number of clients in master area */
static const int resizehints    = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1;    /* 1 will force focus on the fullscreen window */

#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },    /* first entry is default */
	{ "[F]",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[D]",      deck },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },

    /* Last element of layouts array must be NULL, else when cycling though
     * layouts we end up reading element 0 from the rules array. */
    { NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask // Mod1 is alt, mod4 is win
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL }; // "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st",        NULL };

#include "controls.c"

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,             XK_b,      togglebarpos,   {0} },
	{ MODKEY,                       XK_r,      rotatestack,    {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_r,      rotatestack,    {.i = +1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_d,      movestack,      {.i = +1 } },
	{ MODKEY,                       XK_u,      movestack,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_s,      togglermaster,  {0} },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_c,      setmfact_equal, {0} },
	{ MODKEY,                       XK_space,  zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ControlMask,           XK_comma,  cyclelayout,    {.i = -1 } },
	{ MODKEY|ControlMask,           XK_period, cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_c,      setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = 0 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = +1  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
