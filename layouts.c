/* See LICENSE file for copyright and license details.
 *
 * layouts.c — Functions that define window layouts.
 */

void resize_single_client(Monitor *m, Client *c);

void
centeredmaster(Monitor *m)
{
	unsigned int i, n, h, mw, mx, my, oty, ety, tw;
	Client *c;

	/* count number of clients in the selected monitor */
	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if (n == 0)
		return;

	/* initialize areas */
	mw = m->ww;
	mx = 0;
	my = 0;
	tw = mw;

	if (n > m->nmaster) {
		/* go mfact box in the center if more than nmaster clients */
		mw = m->nmaster ? m->ww * m->mfact : 0;
		tw = m->ww - mw;

		if (n - m->nmaster > 1) {
			/* only one client */
			mx = (m->ww - mw) / 2;
			tw = (m->ww - mw) / 2;
		}
	}

	oty = 0;
	ety = 0;
	for (i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
	if (i < m->nmaster) {
		/* nmaster clients are stacked vertically, in the center
		 * of the screen */
		h = (m->wh - my) / (MIN(n, m->nmaster) - i);
		resize(c, m->wx + mx, m->wy + my, mw - (2*c->bw),
		       h - (2*c->bw), 0);
		my += HEIGHT(c);
	} else {
		/* stack clients are stacked vertically */
		if ((i - m->nmaster) % 2 ) {
			h = (m->wh - ety) / ( (1 + n - i) / 2);
			resize(c, m->wx, m->wy + ety, tw - (2*c->bw),
			       h - (2*c->bw), 0);
			ety += HEIGHT(c);
		} else {
			h = (m->wh - oty) / ((1 + n - i) / 2);
			resize(c, m->wx + mx + mw, m->wy + oty,
			       tw - (2*c->bw), h - (2*c->bw), 0);
			oty += HEIGHT(c);
		}
	}
}

void
centeredfloatingmaster(Monitor *m)
{
	unsigned int i, n, w, mh, mw, mx, mxo, my, myo, tx;
	unsigned int g, mgx, sgx; /* gaps, master x + gaps, stack x + gaps */
	Client *c;
	g = m->gappx;

	/* count number of clients in the selected monitor */
	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if (n == 0)
		return;

	if (n == 1){
		c = nexttiled(m->clients);
		resize_single_client(m, c);
		return;
	}

	/* initialize nmaster area */
	if (n > m->nmaster) {
		/* go mfact box in the center if more than nmaster clients */
		if (m->ww > m->wh) {
			mw = m->nmaster ? m->ww * m->mfact : 0;
			mh = m->nmaster ? m->wh * 0.9 : 0;
		} else {
			mh = m->nmaster ? m->wh * m->mfact : 0;
			mw = m->nmaster ? m->ww * 0.9 : 0;
		}
		mgx = mxo = (m->ww - mw) / 2;
		mx = mxo = (m->ww - mw) / 2;
		my = myo = (m->wh - mh) / 2;
	} else {
		/* go fullscreen if all clients are in the master area */
		mh = m->wh;
		mw = m->ww;
		mgx = mxo = 0;
		mx = mxo = 0;
		my = myo = 0;
	}

	for (i = sgx = tx = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
	if (i < m->nmaster) {
		/* nmaster clients are stacked horizontally, in the center
		 * of the screen */
		w = (mw + mxo - mx - (MIN((m->nmaster + 1), n + 1) * g)) / (MIN(n, m->nmaster) - i);
		resize(c,
			m->wx + mgx + g,
			m->wy + my + (m->nmaster >= n ? g : 0),
			w - (2*c->bw),
			mh - (2*c->bw) - (m->nmaster >= n ? (2 * g) : 0),
			0
		);
		mx += WIDTH(c);
		mgx += WIDTH(c) + g;
	} else {
		/* stack clients are stacked horizontally */
		w = (m->ww - tx - ((n - (m->nmaster - 1)) * g)) / (n - i);
		resize(c,
			m->wx + sgx + g,
			m->wy + g,
			w - (2*c->bw),
			m->wh - (2*c->bw) - (2 * g),
			0
		);
		tx += WIDTH(c);
		sgx += WIDTH(c) + g;
	}
}

void
deck(Monitor *m)
{
	unsigned int g, i, n, mw;
	Client *c;
	g = m->gappx;

	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if (n == 0)
		return;

	if (n == 1){
		c = nexttiled(m->clients);
		resize_single_client(m, c);
		return;
	}

	if (n > m->nmaster)
		mw = m->nmaster
			? m->ww * (m->rmaster ? 1.0 - m->mfact : m->mfact)
			: 0;
	else
		mw = m->ww - g;

	for (i = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
		if (i < m->nmaster)
			resize(c,
				m->rmaster ? m->wx + m->ww + g - mw : m->wx + g,
				m->wy + g,
				mw - (2*c->bw) - (m->rmaster ? 2*g : g),
				m->wh - (2*c->bw) - 2*g,
				0
			);
		else
			resize(c,
				m->rmaster ? m->wx + g : m->wx + mw + g,
				m->wy + g,
				m->ww - mw - (2*c->bw) - (m->rmaster ? g : 2*g),
				m->wh - (2*c->bw) - 2*g,
				0
			);
}

void
monocle(Monitor *m)
{
	unsigned int n = 0;
	Client *c;

	for (c = m->clients; c; c = c->next)
		if (ISVISIBLE(c))
			n++;
	if (n > 0) /* override layout symbol */
		snprintf(m->ltsymbol, sizeof m->ltsymbol, "[%d]", n);
	for (c = nexttiled(m->clients); c; c = nexttiled(c->next))
		resize_single_client(m, c);
}

void
resize_single_client(Monitor *m, Client *c) {
	unsigned int g;
	g = m->gappx;
	resize(c,
		m->wx + (smartgaps ? 0 : g),
		m->wy + (smartgaps ? 0 : g),
		m->ww - (2 * c->bw) - (smartgaps ? 0 : 2 * g),
		m->wh - (2 * c->bw) - (smartgaps ? 0 : 2 * g),
		0
	);
}

void
tile(Monitor *m)
{
	unsigned int g, i, n, h, mw, my, ty;
	Client *c;
	g = m->gappx;

	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if (n == 0)
		return;

	if (n == 1){
		c = nexttiled(m->clients);
		resize_single_client(m, c);
		return;
	}

	if (n > m->nmaster)
		mw = m->nmaster
			? m->ww * (m->rmaster ? 1.0 - m->mfact : m->mfact)
			: 0;
	else
		mw = m->ww - g;

	for (i = 0, my = ty = g, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++)
		if (i < m->nmaster) {
			h = (m->wh - my) / (MIN(n, m->nmaster) - i) - g;
			resize(c,
				m->rmaster ? m->wx + m->ww + g - mw : m->wx + g,
				m->wy + my,
				mw - (2*c->bw) - (m->rmaster ? 2*g : g),
				h - (2*c->bw),
				0
			);
			// Prevent crash when too many clients causes client height to becomes negative
			if (my + HEIGHT(c) + g < m->wh)
				my += HEIGHT(c) + g;
		} else {
			h = (m->wh - ty) / (n - i) - g;
			resize(c,
				m->rmaster ? m->wx + g : m->wx + g + mw,
				m->wy + ty,
				m->ww - mw - (2*c->bw) - (m->rmaster ? g : 2*g),
				h - (2*c->bw),
				0
			);
			// Prevent crash when too many clients causes client height to becomes negative
			if (ty + HEIGHT(c) + g < m->wh)
				ty += HEIGHT(c) + g;
		}
}
